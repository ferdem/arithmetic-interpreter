package art;

import java.util.Scanner;

import art.ast.ASTNode;

public class Interpreter {
        
    private Parser parser;

    public Interpreter(Parser parser) {
	this.parser = parser;
    }
    
    public Value interpret() {
	ASTNode root = parser.parse();
	return root.visit();
    }

    public static void main(String[] args) {
	@SuppressWarnings("resource")
	Scanner scanner = new Scanner(System.in);
	while(true) {
	    String line = scanner.nextLine();
	    Lexer lexer = new Lexer(line);
	    Parser parser = new Parser(lexer);
	    Interpreter interpreter = new Interpreter(parser);
	    Value result = interpreter.interpret();
	    System.out.println(result);
	}
    }
    
}

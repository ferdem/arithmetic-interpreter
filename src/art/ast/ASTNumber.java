package art.ast;

import art.Token;
import art.Value;
import art.Token.Type;

public class ASTNumber extends ASTNode {

    public ASTNumber(Token token) {
	super(token);
    }

    @Override
    public Value visit() {
	if (getToken().getType() == Type.INTEGER) {
	    return new Value(getToken().getInteger());
	} else if (getToken().getType() == Type.DOUBLE) {
	    return new Value(getToken().getDouble());
	} else {
	    throw new RuntimeException();
	}
    }

}

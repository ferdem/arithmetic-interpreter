package art.ast;

import art.Value;

public class TypePromotion {
    
    public static boolean doesPromote(Value value) {
	return value.getType() != Value.Type.INTEGER;
    }
    
    public static boolean doesPromote(Value left, Value right) {
 	return doesPromote(left) || doesPromote(right);
     }
    
    public static Value apply(UnaryOperator operator, Value value) {
	if (doesPromote(value)) {
	    return new Value(operator.apply(value.getDouble()));
	} else {
	    return new Value(operator.apply(value.getInteger()));
	}
    }
    
    public static Value apply(BinaryOperator operator, Value left, Value right) {
	if (doesPromote(left, right)) {
	    return new Value(operator.apply(left.getDouble(), 
		    right.getDouble()));
	} else {
	    return new Value(operator.apply(left.getInteger(), 
		    right.getInteger()));
	}
    }

}

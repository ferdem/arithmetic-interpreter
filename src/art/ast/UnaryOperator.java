package art.ast;

public interface UnaryOperator {
    
    public Integer apply(Integer i);
    public Double apply(Double i);
    
    public UnaryOperator NEGATION = new UnaryOperator() {
	@Override
	public Integer apply(Integer i) {
	    return -i;
	}
	@Override
	public Double apply(Double i) {
	    return -i;
	}
    };

}

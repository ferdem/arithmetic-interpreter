package art.ast;

import art.Token;
import art.Value;

public abstract class ASTNode {

    private Token token;
    
    public ASTNode(Token token) {
	this.token = token;
    }
    
    public Token getToken() {
	return token;
    }
    
    public abstract Value visit();
    
}

package art.ast;

import art.Token;
import art.Value;

public class ASTUnaryOperator extends ASTNode {

    private ASTNode child;
    
    public ASTUnaryOperator(Token token, ASTNode child) {
	super(token);
	this.child = child;
    }
    
    public ASTNode getChild() {
	return child;
    }

    @Override
    public Value visit() {
	Value child = getChild().visit();
	switch (getToken().getType()) {
	case MINUS:
	    return TypePromotion.apply(UnaryOperator.NEGATION, child);
	default:
	    throw new RuntimeException();    
	}
    }

}

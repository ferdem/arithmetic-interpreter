package art.ast;

public interface BinaryOperator {
    
    public Integer apply(Integer i, Integer j);
    public Double apply(Double i, Double j);

    public BinaryOperator ADDITION = new BinaryOperator() {
	@Override
	public Integer apply(Integer i, Integer j) {
	    return i + j;
	}
	@Override
	public Double apply(Double i, Double j) {
	    return i + j;
	}
    };

    public BinaryOperator SUBTRACTION = new BinaryOperator() {
	@Override
	public Integer apply(Integer i, Integer j) {
	    return i - j;
	}
	@Override
	public Double apply(Double i, Double j) {
	    return i - j;
	}
    };

    public BinaryOperator MULTIPLICATION = new BinaryOperator() {
	@Override
	public Integer apply(Integer i, Integer j) {
	    return i * j;
	}
	@Override
	public Double apply(Double i, Double j) {
	    return i * j;
	}
    };

    public BinaryOperator DIVISION = new BinaryOperator() {
	@Override
	public Integer apply(Integer i, Integer j) {
	    return i / j;
	}
	@Override
	public Double apply(Double i, Double j) {
	    return i / j;
	}
    };

}

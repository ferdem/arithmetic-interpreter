package art.ast;

import art.Token;
import art.Value;

public class ASTBinaryOperator extends ASTNode {

    private ASTNode leftChild;
    private ASTNode rightChild;
    
    public ASTBinaryOperator(ASTNode left, Token token, ASTNode right) {
	super(token);
	this.leftChild = left;
	this.rightChild = right;
    }
    
    public ASTNode getLeftChild() {
	return leftChild;
    }
    
    public ASTNode getRightChild() {
	return rightChild;
    }

    @Override
    public Value visit() {
	Value left = getLeftChild().visit();
	Value right = getRightChild().visit();
	
	switch (getToken().getType()) {
	case PLUS:
	    return TypePromotion.apply(BinaryOperator.ADDITION, left, right);
	case MINUS:
	    return TypePromotion.apply(BinaryOperator.SUBTRACTION, left, right);
	case ASTERIKS:
	    return TypePromotion.apply(BinaryOperator.MULTIPLICATION, left, right);
	case RIGHT_SLASH:
	    return TypePromotion.apply(BinaryOperator.DIVISION, left, right);
	default:
	    throw new RuntimeException();
	}
    }
    
}

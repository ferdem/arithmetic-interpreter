package art;

public class Lexer {

    private String text;
    private Integer position;

    public Lexer(String text) {
	this.text = text;
	this.position = 0;
    }

    public Boolean isEndOfFile() {
	return position >= text.length();
    }

    public Character cursor() {
	return text.charAt(position);
    }

    public Token getNextToken() {
	while (!isEndOfFile()) {
	    if (Character.isWhitespace(cursor())) {
		skipWhitespace();
		continue;
	    }
	    if (Character.isDigit(cursor())) {
		return getNextNumber();
	    }
	    if (cursor() == '+') {
		position++;
		return new Token(Token.Type.PLUS);
	    }
	    if (cursor() == '-') {
		position++;   
		return new Token(Token.Type.MINUS);
	    }
	    if (cursor() == '*') {
		position++;
		return new Token(Token.Type.ASTERIKS);
	    }
	    if (cursor() == '/') {
		position++;
		return new Token(Token.Type.RIGHT_SLASH);
	    }
	    if (cursor() == '(') {
		position++;
		return new Token(Token.Type.LEFT_PARENTHESIS);
	    }
	    if (cursor() == ')') {
		position++;
		return new Token(Token.Type.RIGHT_PARENTHESIS);
	    }
	    throw new RuntimeException();
	}
	return new Token(Token.Type.END_OF_FILE);
    }

    private void skipWhitespace() {
	while (!isEndOfFile() && Character.isWhitespace(cursor())) {
	    position++;
	}
    }

    private Token getNextNumber() {
	StringBuilder stringBuilder = new StringBuilder();
	while (!isEndOfFile() && Character.isDigit(cursor())) {
	    stringBuilder.append(cursor());
	    position++;
	}
	if (isEndOfFile() || cursor() != '.') {
	    return new Token(Token.Type.INTEGER, Integer.parseInt(stringBuilder.toString()));
	} else {
	    stringBuilder.append(cursor());
	    position++;
	}
	while (!isEndOfFile() && Character.isDigit(cursor())) {
	    stringBuilder.append(cursor());
	    position++;
	}
	return new Token(Token.Type.DOUBLE, Double.parseDouble(stringBuilder.toString()));
    }

}

package art;

import art.ast.ASTBinaryOperator;
import art.ast.ASTNode;
import art.ast.ASTNumber;
import art.ast.ASTUnaryOperator;

public class Parser {    
    
    private Lexer lexer;
    private Token currentToken;
    
    public Parser(Lexer tokenizer) {
	this.lexer = tokenizer;
    }

    private void eat(Token.Type type) {
	if (currentToken.getType() == type) {
	    currentToken = lexer.getNextToken();
	} else {
	    throw new RuntimeException();
	}
    }
    
    public ASTNode parse() {
	currentToken = lexer.getNextToken();
	return expr();
    }
    
    // <expr> ::= <term> {(ADD | SUB) <term>} 
    private ASTNode expr() {
	ASTNode node = term();
	while (currentToken.getType() == Token.Type.PLUS || 
		currentToken.getType() == Token.Type.MINUS) {
	    Token token = currentToken;
	    if (token.getType() == Token.Type.PLUS) {
		eat(Token.Type.PLUS);
	    } else if (token.getType() == Token.Type.MINUS) {
		eat(Token.Type.MINUS);
	    }
	    node = new ASTBinaryOperator(node, token, term());
	}
	return node;
    }
    
    // <term> ::= <factor> {(MUL | DIV) <factor>} 
    private ASTNode term() {
	ASTNode node = signed();
	while (currentToken.getType() == Token.Type.ASTERIKS || 
		currentToken.getType() == Token.Type.RIGHT_SLASH) {
	    Token token = currentToken;
	    if (token.getType() == Token.Type.ASTERIKS) {
		eat(Token.Type.ASTERIKS);
	    } else if (token.getType() == Token.Type.RIGHT_SLASH) {
		eat(Token.Type.RIGHT_SLASH);
	    }
	    node = new ASTBinaryOperator(node, token, signed());
	}
	return node;
    }
    
    // <signed> ::= [-] <unsigned> 
    private ASTNode signed() {
	Token token = currentToken;
	if (token.getType() == Token.Type.MINUS) {
	    eat(Token.Type.MINUS);
	    return new ASTUnaryOperator(token, unsigned());
	}
	return unsigned();
    }

    // <unsigned> ::= (INT | DOUBLE) | [-] LPAREN <expr> RPAREN
    private ASTNode unsigned() {
	Token token = currentToken;
	if (token.getType() == Token.Type.INTEGER) {
	    eat(Token.Type.INTEGER);
	    return new ASTNumber(token);
	}
	if (token.getType() == Token.Type.DOUBLE) {
	    eat(Token.Type.DOUBLE);
	    return new ASTNumber(token);
	}
	eat(Token.Type.LEFT_PARENTHESIS);
	ASTNode node = expr();
	eat(Token.Type.RIGHT_PARENTHESIS);
	
	return node;
    }

}

package art;

public class Value {

    public enum Type {
	INTEGER, DOUBLE
    }

    private Type type;
    private Integer integer;
    private Double doublePrecision;
    
    public Value(Integer integer) {
	this.integer = integer;
	type = Type.INTEGER;
    }

    public Value(Double doublePrecision) {
	this.doublePrecision = doublePrecision;
	type = Type.DOUBLE;
    }

    public Type getType() {
	return type;
    }

    public Integer getInteger() {
	return integer;
    }

    public Double getDouble() {
	return doublePrecision != null ? doublePrecision : integer;
    }

    public String toString() {
	return getType() == Type.INTEGER ? getInteger().toString() 
		: getDouble().toString();
    }

}

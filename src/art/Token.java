package art;

public class Token {
    
    public enum Type {
	INTEGER, DOUBLE, PLUS, MINUS, ASTERIKS, RIGHT_SLASH, 
	LEFT_PARENTHESIS, RIGHT_PARENTHESIS, END_OF_FILE
    }

    private Type type;
    private Double doubleValue;
    private Integer integerValue;

    public Token(Type type) {
	this.type = type;
    }

    public Token(Type type, Double value) {
	this(type);
	doubleValue = value;
    }

    public Token(Type type, Integer value) {
	this(type);
	integerValue = value;
    }

    public Type getType() {
	return type;
    }

    public Integer getInteger() {
	return integerValue;
    }

    public Double getDouble() {
	return doubleValue;
    }

}
